from django.conf import settings
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

PLANS = settings.PLANS.ALL


class Command(BaseCommand):
    def handle(self, *args, **options):
        for plan in PLANS:
            Group.objects.get_or_create(name=plan.name)
            self.stdout.write(self.style.SUCCESS(f'Group "{plan.name}" created'))
