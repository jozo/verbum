from csv import DictReader

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        path = ""
        fieldnames = [
            "user_name",
            "first_name",
            "last_name",
            "email",
            "address_street",
            "address_city",
            "address_zipcode",
            "subscription_starts",
        ]
        with open(path) as f:
            reader = DictReader(f, fieldnames=fieldnames)
            klass = get_user_model()
