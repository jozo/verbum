from django.db import models
from django.db.models import F
from django.shortcuts import redirect
from django.utils.translation import gettext as _
from wagtail.admin import messages
from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register
from wagtail.contrib.settings.models import BaseSetting
from wagtail.contrib.settings.registry import register_setting
from wagtail.core import hooks

from crm.models import Order, Subscriber
from home.models import Article, ArticleIndex, Author


@hooks.register("before_delete_page")
def disable_delete(request, page):
    """Disable deleting of speakers/events

    We don't want it because it breaks urls and connections between speakers and events.
    We disable it only during POST request (so in the final stage of deleting). This means
    delete confirmation page is displayed. From there user can alternatively unpublish the page.
    """
    if (
        request.user.username != "admin"
        and page.specific_class in [Article, Author]
        and request.method == "POST"
    ):
        messages.warning(
            request,
            _("Page '{0}' can not be deleted. You can only unpublish it.").format(
                page.get_admin_display_title()
            ),
        )
        return redirect("wagtailadmin_pages:delete", page.pk)


@register_setting
class SocialMediaSettings(BaseSetting):
    allow_disqus = models.BooleanField(default=True)
    disqus = models.URLField(
        help_text="Your Disqus page URL", default="https://verbumcasopis.disqus.com"
    )


@hooks.register("before_serve_page")
def increment_view_count(page, request, serve_args, serve_kwargs):
    if page.specific_class == ArticleIndex and serve_args and serve_args[1]:
        article_id = serve_args[1][0]
        Article.objects.filter(article_id=article_id).update(
            view_count=F("view_count") + 1
        )


class OrderAdmin(ModelAdmin):
    model = Order
    menu_icon = "pilcrow"  # change as required
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("first_name", "last_name", "email")


class SubscriberAdmin(ModelAdmin):
    model = Subscriber
    menu_icon = "user"
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("first_name", "last_name", "email", "plan")


# Now you just need to register your customised ModelAdmin class with Wagtail
modeladmin_register(OrderAdmin)
modeladmin_register(SubscriberAdmin)
