from home.models import Category, HomePage


def menu(request):
    return {
        "menu": HomePage.objects.first().get_children().in_menu().live(),
        "categories": Category.objects.order_by("title").live().all(),
    }
