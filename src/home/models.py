import stripe
from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import models, transaction
from django.db.models import Max
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import Tag, TaggedItemBase
from wagtail.admin.edit_handlers import (FieldPanel, InlinePanel,
                                         MultiFieldPanel, PageChooserPanel,
                                         StreamFieldPanel)
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.core import blocks
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Orderable, Page
from wagtail.core.wagtail_hooks import check_view_restrictions
from wagtail.embeds.blocks import EmbedBlock
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtailmetadata.models import MetadataPageMixin

from crm.forms import OrderForm, PaywallForm
from crm.models import Order, Subscriber


class HomePage(Page):
    pinned_article = models.ForeignKey(
        "home.Article",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Used as 'Essay of the week'",
    )

    subpage_types = [
        "home.ArticleIndex",
        "home.ArticleTagIndex",
        "home.AuthorIndex",
        "home.CategoryIndex",
        "home.GenericPage",
    ]

    content_panels = Page.content_panels + [
        PageChooserPanel("pinned_article"),
    ]

    def articles(self):
        return Article.objects.live().order_by("-date", "-pk")

    def articles_without_pinned(self):
        return self.articles().exclude(pk=self.pinned_article)

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["article_index"] = (
            self.get_children().type(ArticleIndex).specific().first()
        )
        context["most_viewed"] = self.articles().order_by("-view_count")
        return context


class GenericPage(Page):
    """Provides possibility to create a page with any content"""

    body = RichTextField()

    content_panels = Page.content_panels + [
        FieldPanel("body"),
    ]


class AuthorIndex(Page):
    subpage_types = ["home.Author"]


class Author(MetadataPageMixin, Page):
    first_name = models.CharField(max_length=64, blank=True)
    last_name = models.CharField(max_length=64)
    photo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    description = RichTextField(blank=True)

    schemaorg_type = "Page"
    promote_panels = Page.promote_panels + MetadataPageMixin.panels
    content_panels = Page.content_panels + [
        FieldPanel("first_name"),
        FieldPanel("last_name"),
        ImageChooserPanel("photo"),
        FieldPanel("description"),
    ]

    def name(self):
        return f"{self.first_name} {self.last_name}"

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["articles"] = paginated_articles(
            Article.objects.filter(authors__author=self).live().public(), request
        )
        return context


class Category(Page):
    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        articles = (
            Article.objects.live()
            .filter(article_category=self)
            .order_by("-date", "-pk")
        )
        context["articles"] = paginated_articles(articles, request)
        return context


class CategoryIndex(Page):
    subpage_types = ["home.Category"]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["categories"] = self.get_children().live().public()
        return context


class ArticleTag(TaggedItemBase):
    content_object = ParentalKey(
        "Article", related_name="tagged_items", on_delete=models.CASCADE
    )


class Article(MetadataPageMixin, Page):
    article_id = models.IntegerField(unique=True)
    main_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    date = models.DateField(
        "Post date",
        db_index=True,
        help_text="Slúži len na zobrazenie. "
        "Oddialenie publikovania nájdeš v nastaveniach.",
    )
    article_category = models.ForeignKey(
        "home.Category",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name="articles",
        verbose_name="category",
    )
    tags = ClusterTaggableManager(through=ArticleTag, blank=True)
    view_count = models.PositiveBigIntegerField(default=0, db_index=True)

    free_content = StreamField(
        [
            ("paragraph", blocks.RichTextBlock()),
            ("image", ImageChooserBlock()),
            ("embed", EmbedBlock(help_text="Youtube, Twitter, Vimeo a iné.")),
            (
                "quote",
                blocks.StructBlock(
                    [("text", blocks.TextBlock()), ("author", blocks.CharBlock())],
                    template="home/blocks/quote.html",
                ),
            ),
        ],
        blank=True,
    )
    paid_content = StreamField(
        [
            ("paragraph", blocks.RichTextBlock()),
            ("image", ImageChooserBlock()),
            ("embed", EmbedBlock(help_text="Youtube, Twitter, Vimeo a iné.")),
        ],
        blank=True,
    )

    schemaorg_type = "Page"
    promote_panels = Page.promote_panels + MetadataPageMixin.panels
    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("date"),
                PageChooserPanel("article_category"),
                FieldPanel("tags"),
            ],
            heading="Basic information",
        ),
        ImageChooserPanel("main_image"),
        InlinePanel("authors", heading="authors"),
        StreamFieldPanel("free_content"),
        StreamFieldPanel("paid_content"),
    ]

    parent_page_types = ["home.ArticleIndex"]

    def get_url_parts(self, request=None):
        """Insert article_id to url"""
        site_id, root_url, page_path = super().get_url_parts(request)
        page_path = page_path.split("/")
        page_path.insert(-2, str(self.article_id))
        page_path = "/".join(page_path)
        return site_id, root_url, page_path

    def save(self, clean=True, user=None, log_action=False, **kwargs):
        with transaction.atomic():
            if self.article_id is None:
                max_id = Article.objects.aggregate(max=Max("article_id"))["max"] or 0
                self.article_id = max_id + 1
            return super().save(clean, user, log_action, **kwargs)

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["tags_page"] = ArticleTagIndex.objects.live().last()
        context["paywall_form"] = PaywallForm()
        context["articles_in_category"] = (
            self.article_category.articles.live()
            .public()
            .order_by("-date")
            .exclude(pk=self.pk)
        )
        context["most_viewed"] = Article.objects.live().public().order_by("-view_count")
        return context


class AuthorSequence(Orderable):
    article = ParentalKey(Article, on_delete=models.CASCADE, related_name="authors")
    author = models.ForeignKey(
        Author,
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
        related_name="articles",
    )


class ArticleIndex(RoutablePageMixin, Page):
    subpage_types = ["home.Article"]

    @route(r"^(\d+)/(.+)/")
    def article_with_id_in_url(self, request, article_id, slug):
        article = Article.objects.get(article_id=article_id)
        result = check_view_restrictions(
            article, request, serve_args=None, serve_kwargs=None
        )
        if isinstance(result, HttpResponse):
            return result
        if slug == article.slug:
            return article.serve(request)
        return redirect(article.get_url(request))

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        all_articles = (
            Article.objects.live().public().child_of(self).order_by("-date", "-pk")
        )
        context["articles"] = paginated_articles(all_articles, request)
        return context


class ArticleTagIndex(RoutablePageMixin, Page):
    @route(r"^(.+)/")
    def articles_by_tag(self, request, slug):
        articles = (
            Article.objects.live().filter(tags__slug=slug).order_by("-date", "-pk")
        )
        articles = paginated_articles(articles, request)
        return self.render(
            request,
            template="home/article_index.html",
            context_overrides={"articles": articles},
        )

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["tags"] = Tag.objects.order_by("name").values_list("name", "slug")
        return context


class SubscriptionPage(Page):
    description = RichTextField()
    success_message = RichTextField()

    description_free = RichTextField()
    description_digital = RichTextField()
    description_premium = RichTextField()

    content_panels = Page.content_panels + [
        FieldPanel("description"),
        FieldPanel("description_free"),
        FieldPanel("description_digital"),
        FieldPanel("description_premium"),
        FieldPanel("success_message"),
    ]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["form"] = OrderForm(plan_choices=self.plan_choices())
        context["plans"] = self.plans()
        return context

    def plan_choices(self):
        return settings.PLANS.CHOICES

    def plans(self):
        return {p.code: p for p in settings.PLANS.ALL}

    @transaction.atomic
    def serve(self, request, *args, **kwargs):
        if request.method == "POST":
            return self.serve_payment_process(request, args, kwargs)
        elif request.method == "GET" and "session_id" in request.GET:
            return self.serve_success_payment(request, args, kwargs)
        return super().serve(request, *args, **kwargs)

    def serve_payment_process(self, request, args, kwargs):
        form = OrderForm(self.plan_choices(), request.POST)
        if form.is_valid():
            data = form.cleaned_data
            plan = self.plans()[data["plan"]]
            subscriber = Subscriber(
                username=data["email"],
                email=data["email"],
                first_name=data["first_name"],
                last_name=data["last_name"],
            )
            subscriber.set_password(data["password"])
            subscriber.save()
            subscriber.groups.add(Group.objects.get(name=settings.PLANS.FREE.name))
            if plan == settings.PLANS.FREE:
                return TemplateResponse(
                    request,
                    self.get_template(request, *args, **kwargs),
                    context={
                        "success_message": "Dakujeme za registraciu",
                    },
                )
            else:
                session_id = create_checkout_session(
                    price_id=plan.price_id,
                    success_url=self.full_url,
                    cancel_url=self.full_url,
                )
                subscriber.stripe_session_id = session_id
                Order.objects.create(
                    stripe_session_id=session_id,
                    plan=data["plan"],
                    price=plan.price,
                    first_name=data["first_name"],
                    last_name=data["last_name"],
                    email=data["email"],
                    password=make_password(data["password"]),
                )

                return TemplateResponse(
                    request,
                    self.get_template(request, *args, **kwargs),
                    context={
                        "session_id": session_id,
                        "STRIPE_PUBLISHABLE_KEY": settings.STRIPE_PUBLISHABLE_KEY,
                    },
                )

    def serve_success_payment(self, request, args, kwargs):
        return TemplateResponse(
            request,
            self.get_template(request, *args, **kwargs),
            context={
                "success_message": self.success_message,
            },
        )


def create_checkout_session(price_id, success_url, cancel_url):
    # See https://stripe.com/docs/api/checkout/sessions/create
    # for additional parameters to pass.
    # {CHECKOUT_SESSION_ID} is a string literal; do not change it!
    # the actual Session ID is returned in the query parameter when your customer
    # is redirected to the success page.
    checkout_session = stripe.checkout.Session.create(
        api_key=settings.STRIPE_API_KEY,
        success_url=f"{success_url}?session_id={{CHECKOUT_SESSION_ID}}",
        cancel_url=cancel_url,
        payment_method_types=["card"],
        mode="subscription",
        line_items=[
            {
                "price": price_id,
                # For metered billing, do not pass quantity
                "quantity": 1,
            }
        ],
    )
    return checkout_session["id"]


class Archive(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel("body", classname="full"),
        InlinePanel("issues", label="Issues"),
    ]


class Issue(Orderable):
    article = ParentalKey(Archive, on_delete=models.CASCADE, related_name="issues")
    caption = models.CharField(max_length=64)
    cover = models.ForeignKey(
        "wagtailimages.Image", on_delete=models.CASCADE, related_name="+"
    )
    file = models.FileField()

    panels = [FieldPanel("caption"), ImageChooserPanel("cover"), FieldPanel("file")]


def paginated_articles(all_articles, request):
    paginator = Paginator(all_articles, 10)
    page = request.GET.get("page")
    try:
        # If the page exists and the ?page=x is an int
        articles = paginator.page(page)
    except PageNotAnInteger:
        # If the ?page=x is not an int; show the first page
        articles = paginator.page(1)
    except EmptyPage:
        # If the ?page=x is out of range (too high most likely)
        # Then return the last page
        articles = paginator.page(paginator.num_pages)

    return articles
