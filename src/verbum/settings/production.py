import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from .base import *

DEBUG = False
DJANGO_VITE_DEV_MODE = False


ALLOWED_HOSTS = [os.getenv("ALLOWED_HOST")]
BASE_URL = os.getenv("BASE_URL")

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = "smtp.gmail.com"
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = "redakciaverbum@slh.sk"
EMAIL_HOST_PASSWORD = os.getenv("EMAIL_HOST_PASSWORD")

SECRET_KEY = os.getenv("SECRET_KEY")

sentry_sdk.init(
    dsn=os.getenv("SENTRY_DSN"),
    integrations=[DjangoIntegration()],
    traces_sample_rate=0.0,
    send_default_pii=True,
)

DRAMATIQ_BROKER["OPTIONS"]["url"] = os.getenv("DRAMATIQ_BROKER_URL")
DRAMATIQ_RESULT_BACKEND["BACKEND_OPTIONS"]["url"] = os.getenv("DRAMATIQ_BACKEND_URL")
