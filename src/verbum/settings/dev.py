from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
DJANGO_VITE_DEV_MODE = True

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
SECRET_KEY = "n8=3otg8y6$9ef$4f=mfn1ocf_0kz)!%*!7zw%f83g5#asn(2$"
SENTRY_DSN = ""
