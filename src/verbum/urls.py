from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

import crm.views as crm_views

urlpatterns = [
    path("django-admin/", admin.site.urls),
    path("admin/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    path("login", crm_views.login_view, name="login"),
    path("logout", crm_views.logout_view, name="logout"),
    path("paywall/", crm_views.paywall_registration, name="paywall"),
    path(
        "paywall-finalize/<str:hash_>/",
        crm_views.paywall_finalize,
        name="paywall-finalize",
    ),
    # path("api/crm/mailchimp/", crm.views.mailchimp_change),
    # path("api/stripe/create-checkout-session/", crm_views.create_checkout_session_view),
    path("api/stripe/webhook-received/", crm_views.webhook_received),
    path("", include(wagtail_urls)),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
