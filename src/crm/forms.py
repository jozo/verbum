from django import forms
from django.core.exceptions import ValidationError

from crm.models import Order, User


class UserForm(forms.ModelForm):
    """For registration of user when he buys a subscription"""

    class Meta:
        model = User
        fields = ["first_name", "last_name", "email", "password"]


class OrderForm(forms.ModelForm):
    plan = forms.ChoiceField()

    def __init__(self, plan_choices=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if plan_choices:
            self.fields["plan"].choices = plan_choices

    class Meta:
        model = Order
        fields = ["plan", "first_name", "last_name", "email", "password"]
        widgets = {
            "password": forms.PasswordInput(),
        }


class PaywallForm(forms.Form):
    email = forms.EmailField(
        max_length=255,
        widget=forms.EmailInput(
            attrs={
                "class": "mb-5 p-1 w-full sm:w-9/12 border border-gray-200 text-center",
                "placeholder": "E-mail",
            }
        ),
    )
    target_url = forms.URLField(widget=forms.HiddenInput())

    def clean_email(self):
        email = self.cleaned_data["email"]
        if User.objects.filter(email=email).exists():
            raise ValidationError("Email bol už zaregistrovaný")
        return email


class PaywallFinalizeForm(forms.Form):
    first_name = forms.CharField(
        max_length=255,
        widget=forms.TextInput(
            attrs={
                "class": "mb-5 p-1 w-full sm:w-9/12 border border-gray-200 text-center",
                "placeholder": "Meno",
            }
        ),
    )
    last_name = forms.CharField(
        max_length=255,
        widget=forms.TextInput(
            attrs={
                "class": "mb-5 p-1 w-full sm:w-9/12 border border-gray-200 text-center",
                "placeholder": "Priezvisko",
            }
        ),
    )
    password = forms.CharField(
        max_length=255,
        widget=forms.PasswordInput(
            attrs={
                "class": "mb-5 p-1 w-full sm:w-9/12 border border-gray-200 text-center",
                "placeholder": "Heslo",
            }
        ),
    )


class LoginForm(forms.Form):
    email = forms.EmailField(
        max_length=255,
        widget=forms.EmailInput(
            attrs={
                "class": "mb-5 p-1 w-full sm:w-9/12 border border-gray-200 text-center",
                "placeholder": "E-mail",
            }
        ),
    )
    password = forms.CharField(
        max_length=255,
        widget=forms.PasswordInput(
            attrs={
                "class": "mb-5 p-1 w-full sm:w-9/12 border border-gray-200 text-center",
                "placeholder": "Heslo",
            }
        ),
    )
