from django.conf import settings
from django.contrib.auth.models import AbstractUser, Group
from django.db import models


class User(AbstractUser):
    """User model for admins but also for subscribers"""

    pass


class Subscriber(User):
    """All information we need about a subscriber"""

    mailchimp_id = models.CharField(max_length=64, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    plan = models.CharField(
        max_length=16, choices=settings.PLANS.CHOICES, default=settings.PLANS.FREE.code
    )


class Subscription(models.Model):
    subscriber = models.ForeignKey(User, models.PROTECT)
    order = models.ForeignKey("crm.Order", models.PROTECT, default=None)
    start = models.DateTimeField()
    end = models.DateTimeField()


def get_or_create_subscriber(email) -> User:
    user = User.objects.filter(email=email).last()
    if not user:
        group, _ = Group.objects.get_or_create(name="Subscribers")
        user = User.objects.create_user(username=email, email=email)
        user.groups.add(group)
    return user


class Order(models.Model):
    stripe_session_id = models.CharField(max_length=100)
    plan = models.CharField(
        max_length=16, choices=settings.PLANS.CHOICES, default=settings.PLANS.FREE.code
    )
    price = models.DecimalField(max_digits=5, decimal_places=2)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    password = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class PaywallRegistration(models.Model):
    email = models.EmailField(max_length=255)
    hash = models.CharField(max_length=64)
    target_url = models.URLField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
