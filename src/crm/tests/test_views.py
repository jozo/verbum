import time

import pytest as pytest
import stripe
from django.conf import settings


def generate_header(**kwargs):
    timestamp = kwargs.get("timestamp", int(time.time()))
    payload = kwargs.get("payload", JSON_INVOICE_PAID)
    secret = kwargs.get("secret", settings.STRIPE_WEBHOOK_SECRET)
    scheme = kwargs.get("scheme", stripe.WebhookSignature.EXPECTED_SCHEME)
    signature = kwargs.get("signature", None)
    if signature is None:
        payload_to_sign = "%d.%s" % (timestamp, payload)
        signature = stripe.WebhookSignature._compute_signature(payload_to_sign, secret)
    header = "t=%d,%s=%s" % (timestamp, scheme, signature)
    return header


@pytest.mark.django_db
class TestStripeWebhook:
    def test_invoice_paid(self, client):
        response = client.post(
            "/api/stripe/webhook-received/",
            data=JSON_INVOICE_PAID,
            content_type="application/json",
            HTTP_STRIPE_SIGNATURE=generate_header(),
        )

        assert response.status == 200


STRIPE_SIGNATURE = "t=1617965186,v1=3d99d780e4b9e2e3255e95a1b0bea88cbf80ce49039ebcc85d2c29204746f3be,v0=1308be49a5d018990b8bd652c432e2aae844e59aa640bf18a31c18e8e5fc63c0"

JSON_INVOICE_PAID = """{
  "id": "evt_1IeHr4GdlCXki2g1YKOtoCSj",
  "object": "event",
  "api_version": "2020-08-27",
  "created": 1617965185,
  "data": {
    "object": {
      "id": "in_1IeHr1GdlCXki2g1Ryb5jc9X",
      "object": "invoice",
      "account_country": "SK",
      "account_name": "G3 Test",
      "account_tax_ids": null,
      "amount_due": 99,
      "amount_paid": 99,
      "amount_remaining": 0,
      "application_fee_amount": null,
      "attempt_count": 1,
      "attempted": true,
      "auto_advance": false,
      "billing_reason": "subscription_create",
      "charge": "ch_1IeHr2GdlCXki2g1aea8sdLR",
      "collection_method": "charge_automatically",
      "created": 1617965183,
      "currency": "eur",
      "custom_fields": null,
      "customer": "cus_JGpWkgwTcDQ5kU",
      "customer_address": null,
      "customer_email": "hi@jozo.io",
      "customer_name": null,
      "customer_phone": null,
      "customer_shipping": null,
      "customer_tax_exempt": "none",
      "customer_tax_ids": [
      ],
      "default_payment_method": null,
      "default_source": null,
      "default_tax_rates": [
      ],
      "description": null,
      "discount": null,
      "discounts": [
      ],
      "due_date": null,
      "ending_balance": 0,
      "footer": null,
      "hosted_invoice_url": "https://invoice.stripe.com/i/acct_1IZh9oGdlCXki2g1/invst_JGpWOolQxqSMtwvSnOY6jUoMCDx1BEd",
      "invoice_pdf": "https://pay.stripe.com/invoice/acct_1IZh9oGdlCXki2g1/invst_JGpWOolQxqSMtwvSnOY6jUoMCDx1BEd/pdf",
      "last_finalization_error": null,
      "lines": {
        "object": "list",
        "data": [
          {
            "id": "il_1IeHr1GdlCXki2g1JVmvkkcE",
            "object": "line_item",
            "amount": 99,
            "currency": "eur",
            "description": "1 × Predplatne standard (at €0.99 / year)",
            "discount_amounts": [
            ],
            "discountable": true,
            "discounts": [
            ],
            "livemode": false,
            "metadata": {
            },
            "period": {
              "end": 1649501183,
              "start": 1617965183
            },
            "plan": {
              "id": "price_1IZhQdGdlCXki2g17mRKcUYs",
              "object": "plan",
              "active": true,
              "aggregate_usage": null,
              "amount": 99,
              "amount_decimal": "99",
              "billing_scheme": "per_unit",
              "created": 1616871851,
              "currency": "eur",
              "interval": "year",
              "interval_count": 1,
              "livemode": false,
              "metadata": {
              },
              "nickname": null,
              "product": "prod_JC5bBCjlXuCLb6",
              "tiers_mode": null,
              "transform_usage": null,
              "trial_period_days": null,
              "usage_type": "licensed"
            },
            "price": {
              "id": "price_1IZhQdGdlCXki2g17mRKcUYs",
              "object": "price",
              "active": true,
              "billing_scheme": "per_unit",
              "created": 1616871851,
              "currency": "eur",
              "livemode": false,
              "lookup_key": null,
              "metadata": {
              },
              "nickname": null,
              "product": "prod_JC5bBCjlXuCLb6",
              "recurring": {
                "aggregate_usage": null,
                "interval": "year",
                "interval_count": 1,
                "trial_period_days": null,
                "usage_type": "licensed"
              },
              "tiers_mode": null,
              "transform_quantity": null,
              "type": "recurring",
              "unit_amount": 99,
              "unit_amount_decimal": "99"
            },
            "proration": false,
            "quantity": 1,
            "subscription": "sub_JGpWv4NLMdMgSa",
            "subscription_item": "si_JGpW6TdTs9etpI",
            "tax_amounts": [

            ],
            "tax_rates": [

            ],
            "type": "subscription"
          }
        ],
        "has_more": false,
        "total_count": 1,
        "url": "/v1/invoices/in_1IeHr1GdlCXki2g1Ryb5jc9X/lines"
      },
      "livemode": false,
      "metadata": {
      },
      "next_payment_attempt": null,
      "number": "A4035811-0003",
      "on_behalf_of": null,
      "paid": true,
      "payment_intent": "pi_1IeHr2GdlCXki2g1HnQgncZc",
      "payment_settings": {
        "payment_method_options": null,
        "payment_method_types": null
      },
      "period_end": 1617965183,
      "period_start": 1617965183,
      "post_payment_credit_notes_amount": 0,
      "pre_payment_credit_notes_amount": 0,
      "receipt_number": null,
      "starting_balance": 0,
      "statement_descriptor": null,
      "status": "paid",
      "status_transitions": {
        "finalized_at": 1617965183,
        "marked_uncollectible_at": null,
        "paid_at": 1617965185,
        "voided_at": null
      },
      "subscription": "sub_JGpWv4NLMdMgSa",
      "subtotal": 99,
      "tax": null,
      "total": 99,
      "total_discount_amounts": [
      ],
      "total_tax_amounts": [
      ],
      "transfer_data": null,
      "webhooks_delivered_at": 1617965183
    }
  },
  "livemode": false,
  "pending_webhooks": 1,
  "request": {
    "id": "req_5waEDQbjMjlSga",
    "idempotency_key": null
  },
  "type": "invoice.paid"
}"""
