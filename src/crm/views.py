import hashlib
import json
import logging

import stripe
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.db import transaction
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.utils.crypto import get_random_string
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from crm.forms import LoginForm, PaywallFinalizeForm, PaywallForm
from crm.models import (PaywallRegistration, Subscriber,
                        get_or_create_subscriber)
from crm.tasks import send_email_finish_registration

log = logging.getLogger(__name__)


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(
                request, username=data["email"], password=data["password"]
            )
            if user is not None:
                login(request, user)
                messages.success(request, f"Prihlásenie úspešné")
                return render(request, "home/login.html")
            else:
                form.add_error(
                    field=None,
                    error="Your username and password didn't match. Please try again.",
                )
        return render(request, "home/login.html", {"form": form})
    else:
        form = LoginForm()
        return render(request, "home/login.html", {"form": form})


def logout_view(request):
    logout(request)
    messages.success(request, f"Odhlásenie úspešné")
    return redirect("login")


@require_POST
def paywall_registration(request):
    form = PaywallForm(request.POST)
    if form.is_valid():
        log.info("Form valid")
        data = form.cleaned_data
        pr = PaywallRegistration.objects.create(
            email=data["email"],
            target_url=data["target_url"],
            hash=hashlib.sha256(
                (get_random_string(100) + data["email"]).encode()
            ).hexdigest(),
        )
        url = f"{settings.BASE_URL}/paywall-finalize/{pr.hash}/"
        send_email_finish_registration(data["email"], url)
        messages.success(
            request,
            f"Ďakujeme. Na adresu {data['email']} sme poslali potvrdzovací link.",
        )
        return render(request, "home/paywall.html")
    else:
        return render(request, "home/paywall.html", context={"form": form})


@transaction.atomic
def paywall_finalize(request, hash_):
    pr = PaywallRegistration.objects.filter(hash=hash_).last()
    if not pr:
        form = PaywallFinalizeForm()
        messages.error(
            request,
            f"Poskytnuté ID na finalizáciu registrácie neevidujeme. "
            f"Prosím, skontrolujte URL. {hash_}.",
        )
        return render(
            request, "home/paywall_finalize.html", context={"form": form, "hash": hash_}
        )
    if request.method == "POST":
        form = PaywallFinalizeForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = Subscriber.objects.create(
                username=pr.email,
                email=pr.email,
                first_name=data["first_name"],
                last_name=data["last_name"],
            )
            user.set_password(data["password"])
            user.save()
            pr.delete()
            login(request, user)
            messages.success(
                request,
                "Registrácia dokončená. Ste prihlásený, môžete pokračovať v čítaní Verbumu. "
                "Do 7 sekúnd budete presmerovaný na článok použitý pri registrácii.",
            )
            return render(
                request, "home/paywall_finalize.html", {"redirect_url": pr.target_url}
            )
        else:
            return render(request, "home/paywall_finalize.html", context={"form": form})
    else:
        form = PaywallFinalizeForm()
        return render(
            request, "home/paywall_finalize.html", context={"form": form, "hash": hash_}
        )


@require_POST
@csrf_exempt
def webhook_received(request):
    stripe.api_key = settings.STRIPE_API_KEY
    webhook_secret = settings.STRIPE_WEBHOOK_SECRET
    request_data = json.loads(request.body.decode())

    if webhook_secret:
        # Retrieve the event by verifying the signature using the raw body and secret if webhook signing is configured.
        signature = request.headers.get("stripe-signature")
        try:
            event = stripe.Webhook.construct_event(
                payload=request.body, sig_header=signature, secret=webhook_secret
            )
            data = event["data"]
        except Exception as e:
            return e
        # Get the type of webhook event sent - used to check the status of PaymentIntents.
        event_type = event["type"]
    else:
        data = request_data["data"]
        event_type = request_data["type"]

    if event_type == "checkout.session.completed":
        # Payment is successful and the subscription is created.
        # You should provision the subscription and save the customer ID to your database.
        log.info("session completed: %s", data)
        session_id = data["id"]
        # create account

    elif event_type == "invoice.paid":
        # Continue to provision the subscription as payments continue to be made.
        # Store the status in your database and check when a user accesses your service.
        # This approach helps you avoid hitting rate limits.
        log.info("Invoice paid: %s", data)
        # create subscription
        subscriber = get_or_create_subscriber(email=data["object"]["customer_email"])
        subscriber.create_subscription()
    else:
        log.info("Unhandled event type {}".format(event_type))

    return JsonResponse({"status": "success"})
