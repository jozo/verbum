from django.core.mail import send_mail as django_send_mail
import dramatiq


EMAIL_PAYWALL = """Dobrý deň,

srdečne Vám ďakujem a som veľmi rád, že sa chcete pridať do komunity čitateľov časopisu Verbum.

Verbum tvoríme pre ľudí ako Vy – ľudí, ktorí chcú ísť na hĺbku, majú záujem o kvalitné čítanie, otvorenú diskusiu a neboja sa opačného názoru. Príjmite moje pozvanie byť súčasťou tejto komunity a stať sa spolu s nami tvorcom kultúry.

Na dokončenie Vášho prihlásenia kliknite na tento odkaz a vyplňte Vaše údaje: {}

Vaše údaje budeme spracúvať v súlade so zákonom a nikdy ich neposkytneme nikomu inému.

Ešte raz Vám ďakujem a prajem prijemné čítanie. 

S úctou

Ján Tkáč, kreatívny manažér časopisu Verbum

P. S.: Časopis Verbum si môžete objednať aj v tlačenej podobe v krásnom dizajne dvakrát ročne. Ak máte záujem, napíšte nám na adresu verbum@slh.sk.
"""


@dramatiq.actor(max_retries=5, store_results=True)
def send_email_finish_registration(email, url):
    django_send_mail(
        subject="Dokončite Vaše prihlásenie",
        message=EMAIL_PAYWALL.format(url),
        from_email='"Ján Tkáč, časopis Verbum" <redakciaverbum@slh.sk>',
        recipient_list=[email],
    )
    return "Vsetko prebehlo spravne"
