const {resolve} = require('path');

export default {
    base: '/static/',
    build: {
        manifest: true, // adds a manifest.json
        rollupOptions: {
            // input: [
            //     resolve(__dirname, './main.js'),
            // ]
            input: {
                main: resolve('./main.js')
            }
        },
        outDir: 'static', // puts the manifest.json in PROJECT_ROOT/theme/static/
    },
    plugins: [{
        name: 'reload',
        configureServer(server) {
            const {ws, watcher} = server
            watcher.on('change', file => {
                if (file.endsWith('.html')) {
                    ws.send({
                        type: 'full-reload'
                    })
                }
            })
        }
    }],
    server: {
        port: 3001, // make sure this doesn't conflict with other ports you're using
        open: false,
        watch: {
            paths: './templates/home/**/*.html',
        }
    }
};