# Verbum

A website for magazine Verbum.

## Technologies
 - Backend: Wagtail CMS (Django, Python 3)
 - Frontend: Tailwind CSS
 - Infrastructure: Kubernetes, microk8s

## How to start to develop

### a) Docker-compose
```
docker-compose up
```

### b) Skaffold
*You need to install minikube before*
```
skaffold dev
```

### Frontend
```
cd frontend && npm run dev
```


## TODO
- Infrastructure
  - [ ] CI
  - [ ] Kustomize
  - [x] Monitoring (New Relic?)
- App
  - [ ] Custom error pages
  - [x] Send emails in celery
  - [ ] Create order with Stripe
  - [ ] Add cache


## Commands

```
cd src/ && docker build -t registry.gitlab.com/jozo/verbum:18 .
docker push registry.gitlab.com/jozo/verbum:18
```

```
kubectl apply -n verbum-prod --context verbum-microk8s -f k8s/wagtail.deploy.yaml
```

```
ssh admin@185.185.82.122
microk8s dashboard-proxy
```

```
kubectl port-forward deployment/dramatiq-dashboard 8001:8001 -n verbum-prod --context verbum-microk8s
```

Generate .kube config file. SSH to server and:
```
microk8s config
```